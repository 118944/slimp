# slimp

A simple microscopy SLide image IMPorter. Any slide image that can be read by OpenSlide will be imported into a ZARR store. While other tools like OME's bioformats2raw exist, some specific requirements are not yet implemented.